'use strict';
var express = require('express');
var app = express();
var port = process.env.PORT || 5000;
var sql = require('mssql');
var config = {
    user: 'books',
    password: 'b00ks',
    server: 'localhost\\SQLEXPRESS',
    database: 'books'
};


sql.connect(config, function (err) {
    if (err) {
        console.log(err);
    }
});

app.use(express.static('public'));
app.set(express.static('./src/views'));
app.set('views', './src/views');

/*
var handlebars = require('express-handlebars');
app.engine('.hbs', handlebars({extname: '.hbs'}));
app.set('view engine', '.hbs');
*/

/*
 app.set('view engine', 'jade');
*/

app.set('view engine', 'ejs');

var nav = [{nav: '/Books', text: 'Book'}, {nav: '/Authors', text: 'Author'}];

app.get('/', function (req, res) {
    res.render('index', {
        list: nav
    });
});

var bookRouter = require('./src/routes/bookRoutes')(nav);
app.use('/Books', bookRouter);

var adminRouter = require('./src/routes/adminRoutes')(nav);
app.use('/Admin', adminRouter);

app.get('/authors', function (req, res) {
    res.send('Great authors!');
});

app.listen(port, function (err) {
    console.log(process.env.NODE_ENV);
    console.log('running on server on port ' + port);
});