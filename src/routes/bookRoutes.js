'use strict';

var express = require('express');
var mongodb = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var bookRouter = express.Router();
var sql = require('mssql');

var router = function (nav) {

    bookRouter.get('/', function (req, res) {
        var url = 'mongodb://localhost:27017/libraryApp';
        mongodb.connect(url, function (err, db) {
            if (err) {
                console.log(err);
            } else {
                var collection = db.collection('books');
                collection.find({}).toArray(function (err, results) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render('bookListView', {
                            list: nav,
                            books: results
                        });
                    }
                });
            }
        });
    });

    bookRouter.route('/:id')
        .all(function (req, res, next) {
            var id = new objectId(req.params.id),
                url = 'mongodb://localhost:27017/libraryApp';
            mongodb.connect(url, function (err, db) {
                if (err) {
                    console.log(err);
                } else {
                    var collection = db.collection('books');
                    collection.find({_id: id}).limit(1).next(function (err, results) {
                        if (err) {
                            console.log(err);
                        } else {
                            req.book = results;
                            next();
                        }
                    });
                }
            });
        }).
        get(function (req, res) {
            res.render('bookView', {
                list: nav,
                book: req.book
            });
        });
    return bookRouter;
};

module.exports = router;