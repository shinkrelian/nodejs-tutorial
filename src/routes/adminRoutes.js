'use strict';

var express = require('express');
var mongodb = require('mongodb').MongoClient;
var adminRouter = express.Router();
var books = [
    {title: 'A Game of Thrones', author: 'George R.R. Martin'},
    {title: 'The Lord of the Rings', author: 'J.R.R. Tolkien'},
    {title: 'The Sword of Truth', author: 'Terry Goodkind'},
    {title: 'The Eye of the World', author: 'Robert Jordan'}
];

var router = function (nav) {
    adminRouter.route('/addBooks').
        get(function (req, res) {
            var url = 'mongodb://localhost:27017/libraryApp';
            mongodb.connect(url, function (err, db) {
                var collection = db.collection('books');
                collection.insertMany(books, function (err, results) {
                    db.close();
                    res.send(results);
                });
            });
        });
    return adminRouter;
};

module.exports = router;